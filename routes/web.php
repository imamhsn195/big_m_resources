<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\UpazilaController;
use App\Http\Controllers\DistrictController;
use App\Http\Controllers\UserRegistrationController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('users.index');
});
Route::resource('user_registration', UserRegistrationController::class);
Route::resource('users', UserController::class);
Route::get('getJsonDistricts', [DistrictController::class,'getJsonDistricts'])->name('getJsonDistricts');
Route::get('getJsonUpazila', [UpazilaController::class,'getJsonUpazila'])->name('getJsonUpazila');
Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');
