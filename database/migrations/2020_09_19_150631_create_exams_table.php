<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exams', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->timestamps();
        });
        DB::table('exams')->insert([
            'name' => 'ssc'
        ]);
        DB::table('exams')->insert([
            'name' => 'hsc'
        ]);
        DB::table('exams')->insert([
            'name' => 'honours'
        ]);
        DB::table('exams')->insert([
            'name' => 'masters'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exams');
    }
}
