<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUniversitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('universities', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->timestamps();
        });
        DB::table('universities')->insert([
            'name' => 'Bangladesh Agricultural University'
        ]);        
        DB::table('universities')->insert([
            'name' => 'Chittagong Veterinary and Animal Sciences University'
        ]);
        DB::table('universities')->insert([
            'name' => 'Khulna Agricultural University'
        ]);
        DB::table('universities')->insert([
            'name' => 'Habiganj Agricultural University'
        ]);
        DB::table('universities')->insert([
            'name' => 'International Islamic University Chittagong'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('universities');
    }
}
