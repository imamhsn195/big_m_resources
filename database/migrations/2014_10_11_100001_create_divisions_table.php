<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDivisionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('divisions', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->timestamps();
        });
        DB::table('divisions')->insert([
            'name' => 'Barisal'
        ]);
        DB::table('divisions')->insert([
            'name' => 'Chittagong'
        ]);
        DB::table('divisions')->insert([
            'name' => 'Dhaka'
        ]);
        DB::table('divisions')->insert([
            'name' => 'Khulna'
        ]);
        DB::table('divisions')->insert([
            'name' => 'Rajshahi'
        ]);
        DB::table('divisions')->insert([
            'name' => 'Rangpur'
        ]);
        DB::table('divisions')->insert([
            'name' => 'Sylhet'
        ]);
        DB::table('divisions')->insert([
            'name' => 'Mymensingh'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('divisions');
    }
}
