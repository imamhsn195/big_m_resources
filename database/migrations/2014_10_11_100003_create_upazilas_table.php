<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUpazilasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('upazilas', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->unsignedBigInteger('district_id');
            $table->foreign('district_id')->references('id')->on('districts');
            $table->timestamps();
        });
        DB::table('upazilas')->insert([
            'name' => 'Bandarban Sadar',
            'district_id' => '5'
        ]);
        DB::table('upazilas')->insert([
            'name' => 'Thanchi',
            'district_id' => '5'
        ]);        
        DB::table('upazilas')->insert([
            'name' => 'Lama',
            'district_id' => '5'
        ]);     
        DB::table('upazilas')->insert([
            'name' => 'Naikhongchhari',
            'district_id' => '5'
        ]);
          
        DB::table('upazilas')->insert([
            'name' => 'Badda',
            'district_id' => '9'
        ]);          
        DB::table('upazilas')->insert([
            'name' => 'Banani',
            'district_id' => '9'
        ]);     
        DB::table('upazilas')->insert([
            'name' => 'Bangshal',
            'district_id' => '9'
        ]);        
        DB::table('upazilas')->insert([
            'name' => 'Bhashantek',
            'district_id' => '9'
        ]);        
        DB::table('upazilas')->insert([
            'name' => 'Cantonment',
            'district_id' => '9'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('upazilas');
    }
}
