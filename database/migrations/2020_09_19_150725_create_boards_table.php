<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBoardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('boards', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->timestamps();
        });
        DB::table('boards')->insert([
            'name' => 'Barisal'
        ]);
        DB::table('boards')->insert([
            'name' => 'Chittagong'
        ]);
        DB::table('boards')->insert([
            'name' => 'Dhaka'
        ]);
        DB::table('boards')->insert([
            'name' => 'Khulna'
        ]);
        DB::table('boards')->insert([
            'name' => 'Rajshahi'
        ]);
        DB::table('boards')->insert([
            'name' => 'Rangpur'
        ]);
        DB::table('boards')->insert([
            'name' => 'Sylhet'
        ]);
        DB::table('boards')->insert([
            'name' => 'Mymensingh'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('boards');
    }
}
