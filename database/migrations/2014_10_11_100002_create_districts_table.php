<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDistrictsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('districts', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('division_id');
            $table->foreign('division_id')->references('id')->on('divisions');
            $table->string('name');
            $table->timestamps();
        });
        DB::table('districts')->insert([
            'name' => 'Barguna',
            'division_id' => '1'
        ]);
        DB::table('districts')->insert([
            'name' => 'Barisal',
            'division_id' => '1'
        ]);
        DB::table('districts')->insert([
            'name' => 'Bhola',
            'division_id' => '1'
        ]);
        DB::table('districts')->insert([
            'name' => 'Jhalokati',
            'division_id' => '1'
        ]);
        DB::table('districts')->insert([
            'name' => 'Bandarban',
            'division_id' => '2'
        ]);
        DB::table('districts')->insert([
            'name' => 'Brahmanbaria',
            'division_id' => '2'
        ]);
        DB::table('districts')->insert([
            'name' => 'Chandpur',
            'division_id' => '2'
        ]);
        DB::table('districts')->insert([
            'name' => 'Chittagong',
            'division_id' => '2'
        ]);
        DB::table('districts')->insert([
            'name' => 'Dhaka',
            'division_id' => '3'
        ]);
        DB::table('districts')->insert([
            'name' => 'Faridpur',
            'division_id' => '3'
        ]);
        DB::table('districts')->insert([
            'name' => 'Gazipur',
            'division_id' => '3'
        ]);
        DB::table('districts')->insert([
            'name' => 'Gopalganj',
            'division_id' => '3'
        ]);
        DB::table('districts')->insert([
            'name' => 'Bagerhat',
            'division_id' => '4'
        ]);
        DB::table('districts')->insert([
            'name' => 'Chuadanga',
            'division_id' => '4'
        ]);
        DB::table('districts')->insert([
            'name' => 'Jessore',
            'division_id' => '4'
        ]);
        DB::table('districts')->insert([
            'name' => 'Jhenaidah',
            'division_id' => '4'
        ]);
        DB::table('districts')->insert([
            'name' => 'Bogra',
            'division_id' => '5'
        ]);
        DB::table('districts')->insert([
            'name' => 'Joypurhat',
            'division_id' => '5'
        ]);
        DB::table('districts')->insert([
            'name' => 'Naogaon',
            'division_id' => '5'
        ]);
        DB::table('districts')->insert([
            'name' => 'Natore',
            'division_id' => '5'
        ]);
        DB::table('districts')->insert([
            'name' => 'Dinajpur',
            'division_id' => '6'
        ]);
        DB::table('districts')->insert([
            'name' => 'Gaibandha',
            'division_id' => '6'
        ]);
        DB::table('districts')->insert([
            'name' => 'Kurigram',
            'division_id' => '6'
        ]);
        DB::table('districts')->insert([
            'name' => 'Lalmonirhat',
            'division_id' => '6'
        ]);
        DB::table('districts')->insert([
            'name' => 'Habiganj',
            'division_id' => '7'
        ]);
        DB::table('districts')->insert([
            'name' => 'Maulvibazar',
            'division_id' => '7'
        ]);
        DB::table('districts')->insert([
            'name' => 'Sunamganj',
            'division_id' => '7'
        ]);
        DB::table('districts')->insert([
            'name' => 'Sylhet',
            'division_id' => '7'
        ]);
        DB::table('districts')->insert([
            'name' => 'Jamalpur',
            'division_id' => '8'
        ]);
        DB::table('districts')->insert([
            'name' => 'Mymensingh',
            'division_id' => '8'
        ]);
        DB::table('districts')->insert([
            'name' => 'Netrokona',
            'division_id' => '8'
        ]);
        DB::table('districts')->insert([
            'name' => 'Sherpur',
            'division_id' => '8'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('districts');
    }
}
