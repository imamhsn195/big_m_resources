-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 21, 2020 at 07:45 AM
-- Server version: 8.0.21-0ubuntu0.20.04.4
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `big_m_resources`
--

-- --------------------------------------------------------

--
-- Table structure for table `boards`
--

CREATE TABLE `boards` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `boards`
--

INSERT INTO `boards` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Barisal', NULL, NULL),
(2, 'Chittagong', NULL, NULL),
(3, 'Dhaka', NULL, NULL),
(4, 'Khulna', NULL, NULL),
(5, 'Rajshahi', NULL, NULL),
(6, 'Rangpur', NULL, NULL),
(7, 'Sylhet', NULL, NULL),
(8, 'Mymensingh', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `districts`
--

CREATE TABLE `districts` (
  `id` bigint UNSIGNED NOT NULL,
  `division_id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `districts`
--

INSERT INTO `districts` (`id`, `division_id`, `name`, `created_at`, `updated_at`) VALUES
(1, 1, 'Barguna', NULL, NULL),
(2, 1, 'Barisal', NULL, NULL),
(3, 1, 'Bhola', NULL, NULL),
(4, 1, 'Jhalokati', NULL, NULL),
(5, 2, 'Bandarban', NULL, NULL),
(6, 2, 'Brahmanbaria', NULL, NULL),
(7, 2, 'Chandpur', NULL, NULL),
(8, 2, 'Chittagong', NULL, NULL),
(9, 3, 'Dhaka', NULL, NULL),
(10, 3, 'Faridpur', NULL, NULL),
(11, 3, 'Gazipur', NULL, NULL),
(12, 3, 'Gopalganj', NULL, NULL),
(13, 4, 'Bagerhat', NULL, NULL),
(14, 4, 'Chuadanga', NULL, NULL),
(15, 4, 'Jessore', NULL, NULL),
(16, 4, 'Jhenaidah', NULL, NULL),
(17, 5, 'Bogra', NULL, NULL),
(18, 5, 'Joypurhat', NULL, NULL),
(19, 5, 'Naogaon', NULL, NULL),
(20, 5, 'Natore', NULL, NULL),
(21, 6, 'Dinajpur', NULL, NULL),
(22, 6, 'Gaibandha', NULL, NULL),
(23, 6, 'Kurigram', NULL, NULL),
(24, 6, 'Lalmonirhat', NULL, NULL),
(25, 7, 'Habiganj', NULL, NULL),
(26, 7, 'Maulvibazar', NULL, NULL),
(27, 7, 'Sunamganj', NULL, NULL),
(28, 7, 'Sylhet', NULL, NULL),
(29, 8, 'Jamalpur', NULL, NULL),
(30, 8, 'Mymensingh', NULL, NULL),
(31, 8, 'Netrokona', NULL, NULL),
(32, 8, 'Sherpur', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `divisions`
--

CREATE TABLE `divisions` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `divisions`
--

INSERT INTO `divisions` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Barisal', NULL, NULL),
(2, 'Chittagong', NULL, NULL),
(3, 'Dhaka', NULL, NULL),
(4, 'Khulna', NULL, NULL),
(5, 'Rajshahi', NULL, NULL),
(6, 'Rangpur', NULL, NULL),
(7, 'Sylhet', NULL, NULL),
(8, 'Mymensingh', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `exams`
--

CREATE TABLE `exams` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `exams`
--

INSERT INTO `exams` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'ssc', NULL, NULL),
(2, 'hsc', NULL, NULL),
(3, 'honours', NULL, NULL),
(4, 'masters', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_11_100001_create_divisions_table', 1),
(2, '2014_10_11_100002_create_districts_table', 1),
(3, '2014_10_11_100003_create_upazilas_table', 1),
(4, '2014_10_12_000000_create_users_table', 1),
(5, '2014_10_12_100000_create_password_resets_table', 1),
(6, '2019_08_19_000000_create_failed_jobs_table', 1),
(7, '2020_09_19_150631_create_exams_table', 1),
(8, '2020_09_19_150700_create_universities_table', 1),
(9, '2020_09_19_150725_create_boards_table', 1),
(10, '2020_09_19_150735_create_user_education_table', 1),
(11, '2020_09_19_150808_create_user_trainings_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `universities`
--

CREATE TABLE `universities` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `universities`
--

INSERT INTO `universities` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Bangladesh Agricultural University', NULL, NULL),
(2, 'Chittagong Veterinary and Animal Sciences University', NULL, NULL),
(3, 'Khulna Agricultural University', NULL, NULL),
(4, 'Habiganj Agricultural University', NULL, NULL),
(5, 'International Islamic University Chittagong', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `upazilas`
--

CREATE TABLE `upazilas` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `district_id` bigint UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `upazilas`
--

INSERT INTO `upazilas` (`id`, `name`, `district_id`, `created_at`, `updated_at`) VALUES
(1, 'Bandarban Sadar', 5, NULL, NULL),
(2, 'Thanchi', 5, NULL, NULL),
(3, 'Lama', 5, NULL, NULL),
(4, 'Naikhongchhari', 5, NULL, NULL),
(5, 'Badda', 9, NULL, NULL),
(6, 'Banani', 9, NULL, NULL),
(7, 'Bangshal', 9, NULL, NULL),
(8, 'Bhashantek', 9, NULL, NULL),
(9, 'Cantonment', 9, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `division_id` bigint UNSIGNED NOT NULL,
  `district_id` bigint UNSIGNED NOT NULL,
  `upazila_id` bigint UNSIGNED NOT NULL,
  `languages` json NOT NULL,
  `trainings` json DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `cv` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'secret',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `division_id`, `district_id`, `upazila_id`, `languages`, `trainings`, `address`, `cv`, `photo`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Imam Hasan', 'imamhsn195@gmail.com', 3, 9, 5, '[\"bangla\", \"english\"]', '[[\"Training at Newhorizonz\", \"Training at IDB-BISEW\"], [\"web design and developement\", \"web design and developement\"]]', 'Dhaka', 'cvs/imamHasan_2020-09-21_5f68452ac8e92.pdf', 'photos/imamHasan_2020-09-21_5f68452ac909c.jpg', NULL, 'secret', NULL, '2020-09-21 00:16:10', '2020-09-21 00:16:10'),
(2, 'Abu Bakar', 'admin@asiafashion.com', 2, 5, 1, '[\"bangla\", \"english\"]', NULL, 'ctg', 'cvs/abuBakar_2020-09-21_5f684624d64fc.pdf', 'photos/abuBakar_2020-09-21_5f684624d6674.jpg', NULL, 'secret', NULL, '2020-09-21 00:20:20', '2020-09-21 00:20:20'),
(3, 'Imam Hasanw', 'imamhssn195@gmail.com', 3, 9, 6, '[\"bangla\", \"english\"]', '[[\"Training at Newhorizonz\", \"Training at IDB-BISEW\"], [\"web design and developement\", \"web design and developement\"]]', 'Dhaka', 'cvs/imamHasan_2020-09-21_5f68452ac8e92.pdf', 'photos/imamHasan_2020-09-21_5f68452ac909c.jpg', NULL, 'secret', NULL, '2020-09-21 00:16:10', '2020-09-21 00:16:10'),
(4, 'Abu Bakarawea', 'adminawda@asiafashion.com', 2, 5, 2, '[\"bangla\", \"english\"]', NULL, 'ctg', 'cvs/abuBakar_2020-09-21_5f684624d64fc.pdf', 'photos/abuBakar_2020-09-21_5f684624d6674.jpg', NULL, 'secret', NULL, '2020-09-21 00:20:20', '2020-09-21 00:20:20'),
(5, 'Imam Hasanasda', 'asdfasimamhsn195@gmail.com', 3, 9, 7, '[\"bangla\", \"english\"]', '[[\"Training at Newhorizonz\", \"Training at IDB-BISEW\"], [\"web design and developement\", \"web design and developement\"]]', 'Dhaka', 'cvs/imamHasan_2020-09-21_5f68452ac8e92.pdf', 'photos/imamHasan_2020-09-21_5f68452ac909c.jpg', NULL, 'secret', NULL, '2020-09-21 00:16:10', '2020-09-21 00:16:10'),
(6, 'Abu Bakarasdfasd', 'aadmin@asiaasdffashion.com', 2, 5, 3, '[\"bangla\", \"english\"]', NULL, 'ctg', 'cvs/abuBakar_2020-09-21_5f684624d64fc.pdf', 'photos/abuBakar_2020-09-21_5f684624d6674.jpg', NULL, 'secret', NULL, '2020-09-21 00:20:20', '2020-09-21 00:20:20'),
(7, 'Imam asdfasHasanw', 'imamhssnasdfas195@gmail.com', 2, 5, 4, '[\"bangla\", \"english\"]', '[[\"Training at Newhorizonz\", \"Training at IDB-BISEW\"], [\"web design and developement\", \"web design and developement\"]]', 'Dhaka', 'cvs/imamHasan_2020-09-21_5f68452ac8e92.pdf', 'photos/imamHasan_2020-09-21_5f68452ac909c.jpg', NULL, 'secret', NULL, '2020-09-21 00:16:10', '2020-09-21 00:16:10'),
(8, 'Abu Bakarawasasea', 'adminasdfawda@asiafashion.com', 3, 9, 6, '[\"bangla\", \"english\"]', NULL, 'ctg', 'cvs/abuBakar_2020-09-21_5f684624d64fc.pdf', 'photos/abuBakar_2020-09-21_5f684624d6674.jpg', NULL, 'secret', NULL, '2020-09-21 00:20:20', '2020-09-21 00:20:20'),
(9, 'Imam Hasan', 'imaasdfadfmhsn195@gmail.com', 3, 9, 5, '[\"bangla\", \"english\"]', '[[\"Training at Newhorizonz\", \"Training at IDB-BISEW\"], [\"web design and developement\", \"web design and developement\"]]', 'Dhaka', 'cvs/imamHasan_2020-09-21_5f68452ac8e92.pdf', 'photos/imamHasan_2020-09-21_5f68452ac909c.jpg', NULL, 'secret', NULL, '2020-09-21 00:16:10', '2020-09-21 00:16:10'),
(10, 'Abu Bakar', 'adasefasdfmin@asiafashion.com', 2, 5, 1, '[\"bangla\", \"english\"]', NULL, 'ctg', 'cvs/abuBakar_2020-09-21_5f684624d64fc.pdf', 'photos/abuBakar_2020-09-21_5f684624d6674.jpg', NULL, 'secret', NULL, '2020-09-21 00:20:20', '2020-09-21 00:20:20'),
(11, 'Imam Hasanw', 'imamhsqwerqwrsn195@gmail.com', 3, 9, 6, '[\"bangla\", \"english\"]', '[[\"Training at Newhorizonz\", \"Training at IDB-BISEW\"], [\"web design and developement\", \"web design and developement\"]]', 'Dhaka', 'cvs/imamHasan_2020-09-21_5f68452ac8e92.pdf', 'photos/imamHasan_2020-09-21_5f68452ac909c.jpg', NULL, 'secret', NULL, '2020-09-21 00:16:10', '2020-09-21 00:16:10'),
(12, 'Abu Bakarawea', 'admiqweqwrgnawda@asiafashion.com', 2, 5, 2, '[\"bangla\", \"english\"]', NULL, 'ctg', 'cvs/abuBakar_2020-09-21_5f684624d64fc.pdf', 'photos/abuBakar_2020-09-21_5f684624d6674.jpg', NULL, 'secret', NULL, '2020-09-21 00:20:20', '2020-09-21 00:20:20'),
(13, 'Imam Hasanasda', 'asdfaerersimamhsn195@gmail.com', 3, 9, 7, '[\"bangla\", \"english\"]', '[[\"Training at Newhorizonz\", \"Training at IDB-BISEW\"], [\"web design and developement\", \"web design and developement\"]]', 'Dhaka', 'cvs/imamHasan_2020-09-21_5f68452ac8e92.pdf', 'photos/imamHasan_2020-09-21_5f68452ac909c.jpg', NULL, 'secret', NULL, '2020-09-21 00:16:10', '2020-09-21 00:16:10'),
(14, 'Abu Bakarasdfasd', 'aadermin@asiaasdffashion.com', 2, 5, 3, '[\"bangla\", \"english\"]', NULL, 'ctg', 'cvs/abuBakar_2020-09-21_5f684624d64fc.pdf', 'photos/abuBakar_2020-09-21_5f684624d6674.jpg', NULL, 'secret', NULL, '2020-09-21 00:20:20', '2020-09-21 00:20:20'),
(15, 'Imam asdfasHasanw', 'imamhrdssnasdfas195@gmail.com', 2, 5, 4, '[\"bangla\", \"english\"]', '[[\"Training at Newhorizonz\", \"Training at IDB-BISEW\"], [\"web design and developement\", \"web design and developement\"]]', 'Dhaka', 'cvs/imamHasan_2020-09-21_5f68452ac8e92.pdf', 'photos/imamHasan_2020-09-21_5f68452ac909c.jpg', NULL, 'secret', NULL, '2020-09-21 00:16:10', '2020-09-21 00:16:10'),
(16, 'Abu Bakarawasasea', 'adminawerasdfawda@asiafashion.com', 3, 9, 6, '[\"bangla\", \"english\"]', NULL, 'ctg', 'cvs/abuBakar_2020-09-21_5f684624d64fc.pdf', 'photos/abuBakar_2020-09-21_5f684624d6674.jpg', NULL, 'secret', NULL, '2020-09-21 00:20:20', '2020-09-21 00:20:20');

-- --------------------------------------------------------

--
-- Table structure for table `user_education`
--

CREATE TABLE `user_education` (
  `id` bigint UNSIGNED NOT NULL,
  `user_id` bigint UNSIGNED NOT NULL,
  `exam_id` bigint UNSIGNED NOT NULL,
  `university_id` bigint UNSIGNED NOT NULL,
  `board_id` bigint UNSIGNED NOT NULL,
  `exam_result` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_education`
--

INSERT INTO `user_education` (`id`, `user_id`, `exam_id`, `university_id`, `board_id`, `exam_result`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 1, '4.8', '2020-09-21 00:16:10', '2020-09-21 00:16:10'),
(2, 1, 2, 2, 2, '4.2', '2020-09-21 00:16:10', '2020-09-21 00:16:10'),
(3, 2, 1, 1, 1, '4', '2020-09-21 00:20:20', '2020-09-21 00:20:20'),
(4, 2, 2, 2, 2, '4', '2020-09-21 00:20:20', '2020-09-21 00:20:20');

-- --------------------------------------------------------

--
-- Table structure for table `user_trainings`
--

CREATE TABLE `user_trainings` (
  `id` bigint UNSIGNED NOT NULL,
  `user_id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `boards`
--
ALTER TABLE `boards`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `districts`
--
ALTER TABLE `districts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `districts_division_id_foreign` (`division_id`);

--
-- Indexes for table `divisions`
--
ALTER TABLE `divisions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `exams`
--
ALTER TABLE `exams`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `universities`
--
ALTER TABLE `universities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `upazilas`
--
ALTER TABLE `upazilas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `upazilas_district_id_foreign` (`district_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_division_id_foreign` (`division_id`),
  ADD KEY `users_district_id_foreign` (`district_id`),
  ADD KEY `users_upazila_id_foreign` (`upazila_id`);

--
-- Indexes for table `user_education`
--
ALTER TABLE `user_education`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_education_user_id_foreign` (`user_id`),
  ADD KEY `user_education_exam_id_foreign` (`exam_id`),
  ADD KEY `user_education_university_id_foreign` (`university_id`),
  ADD KEY `user_education_board_id_foreign` (`board_id`);

--
-- Indexes for table `user_trainings`
--
ALTER TABLE `user_trainings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_trainings_user_id_foreign` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `boards`
--
ALTER TABLE `boards`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `districts`
--
ALTER TABLE `districts`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `divisions`
--
ALTER TABLE `divisions`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `exams`
--
ALTER TABLE `exams`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `universities`
--
ALTER TABLE `universities`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `upazilas`
--
ALTER TABLE `upazilas`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `user_education`
--
ALTER TABLE `user_education`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `user_trainings`
--
ALTER TABLE `user_trainings`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `districts`
--
ALTER TABLE `districts`
  ADD CONSTRAINT `districts_division_id_foreign` FOREIGN KEY (`division_id`) REFERENCES `divisions` (`id`);

--
-- Constraints for table `upazilas`
--
ALTER TABLE `upazilas`
  ADD CONSTRAINT `upazilas_district_id_foreign` FOREIGN KEY (`district_id`) REFERENCES `districts` (`id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_district_id_foreign` FOREIGN KEY (`district_id`) REFERENCES `districts` (`id`),
  ADD CONSTRAINT `users_division_id_foreign` FOREIGN KEY (`division_id`) REFERENCES `divisions` (`id`),
  ADD CONSTRAINT `users_upazila_id_foreign` FOREIGN KEY (`upazila_id`) REFERENCES `upazilas` (`id`);

--
-- Constraints for table `user_education`
--
ALTER TABLE `user_education`
  ADD CONSTRAINT `user_education_board_id_foreign` FOREIGN KEY (`board_id`) REFERENCES `boards` (`id`),
  ADD CONSTRAINT `user_education_exam_id_foreign` FOREIGN KEY (`exam_id`) REFERENCES `exams` (`id`),
  ADD CONSTRAINT `user_education_university_id_foreign` FOREIGN KEY (`university_id`) REFERENCES `universities` (`id`),
  ADD CONSTRAINT `user_education_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `user_trainings`
--
ALTER TABLE `user_trainings`
  ADD CONSTRAINT `user_trainings_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
