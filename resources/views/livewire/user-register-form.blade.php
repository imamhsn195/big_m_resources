<div>
   <x-jet-form-section submit=submit formClasses="grid grid-cols-2 gap-4" >
       <x-slot name="title">
            User Register Form
       </x-slot>
       <x-slot name="description">
            User Register Form using Laravel 8 JetStream, Livewire, tailwindcss
       </x-slot>
       <x-slot name="form">
           <div>
            <x-jet-label value="Name"></x-jet-label>
            <x-jet-input wire:model="name"></x-jet-input>
            @error('name') <span class="error">{{ $message }}</span> @enderror
           </div>
           <div>
            <x-jet-label value="Email"></x-jet-label>
            <x-jet-input wire:model="email"></x-jet-input>
            @error('email') <span class="error">{{ $message }}</span> @enderror
           </div>
       </x-slot>
       <x-slot name="actions">
           <x-jet-danger-button type="reset" class="">Reset</x-jet-danger-button>
           <x-jet-button class="" submit="submit">Register</x-jet-button>
       </x-slot>
   </x-jet-form-section>
</div>
