@extends('layouts.master')

@section('content')
<div class="container">
<div class="row">
    <form class="form-inline" action="{{route('users.index')}}" method="GET">
        <div class="form-group  mx-sm-2">
            <label for="" class="sr-only">Name</label>
            <input type="text" class="form-control" name="name" id="name" value="" placeholder="Name">
        </div>
        <div class="form-group mx-sm-2">
            <label for="" class="sr-only">Email</label>
            <input type="email" class="form-control" name="email" id="email" value="" placeholder="Email">
        </div>
        <div class="form-group  mx-sm-2">
            <label for="division_id" class="sr-only">Division</label>
            <select id="division_id" name="division_id" class="form-control">
                <option value="all" selected>All Devision</option>
                    @forelse($divisions as $division)
                        <option value="{{$division->id}}">{{$division->name}}</option>
                        @empty
                    @endforelse
            </select>
        </div>
        <div class="form-group  mx-sm-2">
            <label for="district_id" class="sr-only">District</label>
            <select name="district_id" id="district_id" class="form-control">
                <option value="all" selected>All District</option>

                {{--  @forelse($districts as $district)
                    <option value="{{$district->id}}">{{$district->name}}</option>
                @empty
                @endforelse  --}}
            </select>
        </div>
        <div class="form-group  mx-sm-2">
            <label for="upazila_id"  class="sr-only">Upazila/Thana</label>
            <select id="upazila_id" name="upazila_id" class="form-control">
                <option value="all" selected>Choose Upazila/Thana</option>
                {{--  @forelse($upazilas as $upazila)
                    <option value="{{$upazila->id}}">{{$upazila->name}}</option>
                @empty
                @endforelse  --}}
            </select>
        </div>
        <div class="form-group  mx-sm-1">
            <button type="submit" class="btn btn-primary mb-2">Search</button>
        </div>
        <div class="form-group  mx-sm-12">
            <a href="{{ route('users.create')}}" class="btn btn-success mb-2">Add New</a>
            <a href="{{ route('users.index')}}" class="btn btn-success mb-2">View All</a>
        </div>
    </form>
</div>       
</div>
<div class="container">
    <div class="row">
    <table class="table table-bordered">
        <thead>
        <tr>
            <th scope="col">#SL</th>
            <th scope="col">Applicant Name</th>
            <th scope="col">Email</th>
            <th scope="col">Division</th>
            <th scope="col">District</th>
            <th scope="col">Upazila/Thana</th>
            <th scope="col">Intested Date</th>
            <th scope="col">Actions</th>
        </tr>
        </thead>
        <tbody>
            @forelse($users as $key => $user)
            <tr>
                <td> {{(($users->currentPage() - 1) * $users->perPage() + $key+1)}} </td>
                <td>{{$user->name}}</td>
                <td>{{$user->email}}</td>
                <td>{{$user->division->name}}</td>
                <td>{{$user->district->name}}</td> 
                <td>{{$user->upazila->name}}</td>
                <td>{{date('d-m-Y', strtotime($user->created_at))}}</td>
                <td><a href="{{route('users.edit',$user->id)}}">Edit</a></td>
            </tr>
            @empty

            @endforelse
        </tbody>
    </table>
</div>
{{ $users->withQueryString()->links() }}
</div>
@endsection

@once
@push('javascript')
    <script>
        $('#division_id').on('change',function(){
            let division_id = $('#division_id').val();
            let url = "{{route('getJsonDistricts')}}";
            $.ajax({url: url + "?division_id=" +division_id, success: function(result){
                let district_op = "<option selected>Choose...</option>";
                result.forEach(element => {
                    district_op += "<option value='"+ element.id +"'>" + element.name + "</option>";
                });
                $('#district_id').empty().append(district_op);
              }});
        });
        $('#district_id').on('change',function(){
            let district_id = $('#district_id').val();
            let url = "{{route('getJsonUpazila')}}";
            $.ajax({url: url + "?district_id=" + district_id, success: function(result){
                let upazila_op = "<option selected>Choose...</option>";
                result.forEach(element => {
                    upazila_op += "<option value='"+ element.id +"'>" + element.name + "</option>";
                });
                $('#upazila_id').empty().append(upazila_op);
              }});
        });
    </script>
@endpush
@endonce