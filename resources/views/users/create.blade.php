@extends('layouts.master')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>User Registration</h2>
                <form action="{{route('users.store')}}" method="POST" id="create_user_form" enctype="multipart/form-data">
                    @csrf
                    <div class="form-row">
                      <div class="form-group col-md-6">
                        <label for="name">Name</label>
                        <input type="text" name="name" class="form-control" id="name">
                      </div>
                      <div class="form-group col-md-6">
                        <label for="email">Email</label>
                        <input type="email" name="email" class="form-control" id="email">
                      </div>
                    </div>
                    <div class="form-row">
                      <div class="form-group col-md-4">
                        <label for="division_id">Division</label>
                        <select id="division_id" name="division_id" class="form-control">
                          <option selected>Choose...</option>
                            @forelse($divisions as $division)
                                <option value="{{$division->id}}">{{$division->name}}</option>
                                @empty
                            @endforelse
                        </select>
                      </div>
                      <div class="form-group col-md-4">
                        <label for="district_id">District</label>
                        <select name="district_id" id="district_id" class="form-control">
                          <option selected>Choose...</option>
                          <option>...</option>
                        </select>
                      </div>
                      <div class="form-group col-md-4">
                        <label for="upazila_id">Upazila/Thana</label>
                        <select name="upazila_id" id="upazila_id" class="form-control">
                          <option selected>Choose...</option>
                          <option>...</option>
                        </select>
                      </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-12">
                        <label for="inputAddress">Address</label>
                        <textarea name="address" id="inputAddress" cols="30" rows="5" class="form-control"></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group 12">
                            <lagbel class="form-check col">Languae Proficiancy</label>
                        <div class="form-check col">
                            <input name="languages[]" class="form-check-input" type="checkbox" id="bangla" value="bangla">
                            <label class="form-check-label" for="bangla">Bangla</label>
                        </div>
                        <div class="form-check col">
                            <input name="languages[]" class="form-check-input" type="checkbox" id="english" value="english">
                            <label class="form-check-label" for="english">English</label>
                        </div>
                        <div class="form-check col">
                            <input name="languages[]" class="form-check-input" type="checkbox" id="french" value="french">
                            <label class="form-check-label" for="french">French</label>
                        </div>
                        </div>                
                    </div>
                    <div class="row">
                        <div class="form-group col-md-4">
                                <label for="inputUpazila" >Photo <span>(Upload (Only Allow Image))</span></label>
                                <input type="file" id="photo" name="photo" onchange="readURL(this,'photo_preview')"  class="form-control">
                            </div>
                            <div class="col-md-2 text-center">
                                <img id="photo_preview" style="display:none;" class="img-circle elevation-2" src="" alt="Profile Picture" width="100px" height="100px" />
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputUpazila">CV <span>(Upload (Only Allow DOC/PDF))</span></label>
                                <input type="file" name="cv" class="form-control">
                            </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-3">
                        <label for="exam_name">Exam Name</label>
                        <select id="exam_name" name="exam_name" class="form-control">
                            <option value="" selected>Choose...</option>
                            @forelse($exams as $exam)
                                <option value="{{$exam->id}}">{{ucFirst($exam->name)}}</option>
                            @empty
        
                            @endforelse
                        </select>
                        </div>
                        <div class="form-group col-md-3">
                        <label for="university_name">University</label>
                        <select id="university_name" class="form-control">
                            <option value="" selected>Choose...</option>
                            @forelse($universities as $university)
                                <option value="{{$university->id}}">{{ucFirst($university->name)}}</option>
                            @empty
        
                            @endforelse
                        </select>
                        </div>
                        <div class="form-group col-md-3">
                        <label for="board_name">Board</label>
                        <select id="board_name" class="form-control">
                            <option value="" selected>Choose...</option>
                            @forelse($boards as $board)
                                <option value="{{$board->id}}">{{$board->name}}</option>
                            @empty
        
                            @endforelse
                        </select>
                        </div>
                        <div class="form-group col-md-2">
                        <label for="result" >Result</label>
                        <input type="text" value="" id="exam_result" class="form-control">
                    </div>
                    <div class="form-group col-md-1">
                        <label for="result" >&nbsp;</label><br/>
                        <input type="button" name="" id="add_exam_button" class="btn btn-info" value="Add Exam">
                    </div></div>
                    <div class="form-row">
                        <label for="result">Exam List<a class="btn btn-link text-success"></a></label>
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th scope="col">Exam</th>
                                <th scope="col">University</th>
                                <th scope="col">Board</th>
                                <th scope="col">Result</th>
                                <th scope="col">Actions</th>
                            </tr>
                            </thead>
                            <tbody id="exam_table_body"></tbody>
                        </table>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-check col-md-5">
                        <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="have_training" name="have_training" value="have_training" class="custom-control-input">
                        <label class="custom-control-label" for="have_training">I have training</label>
                      </div>
                      <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="dont_have_training" name="have_training" value="dont_have_training" class="custom-control-input">
                        <label class="custom-control-label" for="dont_have_training">I do not have training</label>
                      </div>
                    </div>
                    <div class="form-check col-md-7 mb-3">
                        <div id="training_section" style="display: none">
                            <div class="form-row" >
                                <div class="form-group col-md-4">
                                    <label for="training_name">Traing Name</label>
                                    <input type="text" id="training_name" class="form-control">
                                </div>
                                <div class="form-group col-md-5">
                                    <label for="training_details">Training Details</label>
                                    <input type="text" id="training_details" class="form-control">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="result"> &nbsp;</label><br/>
                                    <input type="button" id="add_training_button" class="btn btn-info" value="Add Training Details">
                                </div>
                            </div>
                            <label for="result">Training List</label>
                            <table class="table">
                                <thead>
                                <tr>
                                    <th scope="col">Training Name</th>
                                    <th scope="col">Training Details</th>
                                    <th scope="col">Actions</th>
                                </tr>
                                </thead>
                                <tbody id="training_table_body"></tbody>
                            </table>
                        </div>
                        
                    </div>
                    <button type="submit" class="btn btn-primary">Submit Form</button>
                  </form>
            </div>
        </div>
    </div>
@endsection
@once
    @push('javascript')
        <script>
            $('#division_id').on('change',function(){
                let division_id = $('#division_id').val();
                let url = "{{route('getJsonDistricts')}}";
                $.ajax({url: url + "?division_id=" +division_id, success: function(result){
                    let district_op = "<option selected>Choose...</option>";
                    result.forEach(element => {
                        district_op += "<option value='"+ element.id +"'>" + element.name + "</option>";
                    });
                    $('#district_id').empty().append(district_op);
                  }});
            });
            $('#district_id').on('change',function(){
                let district_id = $('#district_id').val();
                let url = "{{route('getJsonUpazila')}}";
                $.ajax({url: url + "?district_id=" + district_id, success: function(result){
                    let upazila_op = "<option selected>Choose...</option>";
                    result.forEach(element => {
                        upazila_op += "<option value='"+ element.id +"'>" + element.name + "</option>";
                    });
                    $('#upazila_id').empty().append(upazila_op);
                  }});
            });
            $(document).ready(function() {
                $("#add_exam_button").click(function(){
                    var exam_name = $("#exam_name option:selected");
                    var university_name = $("#university_name option:selected");
                    var board_name = $("#board_name option:selected");
                    var result = $("#exam_result");
                    if(exam_name.val() == "" || university_name.val() == "" || board_name.val() == "" || result.val() == "") {
                        alert("Please Fill all Exam info");
                        return;
                    }
                    var markup = "<tr>";
                        markup += "<td>"+exam_name.text()+"<input type='hidden' name='exam_name[]' value='"+exam_name.val()+"'/></td>";
                        markup += "<td>"+university_name.text()+"<input type='hidden' name='university_name[]' value='"+university_name.val()+"'/></td>";
                        markup += "<td>"+board_name.text()+"<input type='hidden' name='board_name[]' value='"+board_name.val()+"'/></td>";
                        markup += "<td>"+result.val()+"<input type='hidden' name='exam_result[]' value='"+result.val()+"'/></td>";
                        markup += "<td><input type='button' class='btn btn-danger remove' value='Delete Exam'></td>";
                        
                        markup += "</tr>";
                        
                    $("#exam_table_body").append(markup);
                });
            });
            $(document).ready(function(){
                $("input[name='have_training']").click(function(){
                    var radioValue = $("input[name='have_training']:checked").val();
                    if(radioValue == "have_training"){
                         $('#training_section').show(400); 
                    }else{
                         $('#training_section').hide(400); 
                    }
                });
            });

            $(document).ready(function() {
                $("#add_training_button").click(function(){
                    var training_name = $('#training_name');
                    var training_details = $('#training_details');

                    if(training_name.val() == "" || training_details.val() == "") {
                        alert("Please Fill all Training info");
                        return;
                    }
                    var training_tr = "<tr>";
                        training_tr += "<td>"+training_name.val()+`<input type='hidden' name='training_names[]' value='`+training_name.val()+"'></td>";
                        training_tr += "<td>"+training_details.val()+`<input type='hidden' name='training_details[]' value='`+training_details.val()+"'> </td>";
                        training_tr += "<td><input type='button' id='remove_training' value='Remove' class='btn btn-danger remove'></td>";
                        training_tr += "</tr>";
                    $('#training_table_body').append(training_tr);
                });
            });
            
            $('#create_user_form').submit(function(e){
                e.preventDefault(e);
                $.ajaxSetup({
                    header:$('meta[name="_token"]').attr('content')
                })
                var data = new FormData(this);
                document.getElementById('training_section')
                .querySelectorAll('input[type=hidden]')
                .forEach(function(e){
                    data.append(e.name,e.value);
                });

                $.ajax({
                    data:  data,
                    contentType: false,
                        cache: false,
                    processData:false,
                    type:"POST",
                    url:"{{route('users.index')}}",
                    dataType: 'json',
                    success: function(data){
                        $('.alert').removeClass('alert-danger');
                        $('.alert').addClass('show alert-success');
                        $('.alert h2').text(data.message);
                    },
                    error: function(error){
                        var errorString = '';
                        err = error.responseJSON.errors;
                        $.each( err, function( key, value) {
                            errorString += '<li>' + value + '</li>';
                        });
                        $('.alert').addClass('show');
                        $('.alert h2').text("Validation failed");
                        $('.alert ul').empty().append(errorString);
                    }
                });
            });

            function readURL(input,previewId) {
                if (input.files && input.files[0]) {
                  var reader = new FileReader();
                  
                  reader.onload = function(e) {
                    $('#'+previewId).attr('src', e.target.result);
                    $('#'+previewId).css('display', 'block');
                  }
                  reader.readAsDataURL(input.files[0]); // convert to base64 string
                }
              } 

              $(document).ready(function() {
                $('table').on('click','tr input.remove',function(e){
                    e.preventDefault();
                    $(this).closest('tr').remove();
                });                
            });
        </script>
    @endpush
@endonce