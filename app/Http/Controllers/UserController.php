<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Exam;
use App\Models\User;
use App\Models\Board;
use App\Models\Upazila;
use App\Models\District;
use App\Models\Division;
use App\Models\University;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\UserEducation;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::with('division','district','upazila')
         ->where(function($query){
             if(request('name') && request('name') !==''){
                $query->orWhere('name','like',"%".request('name')."%");
             }
             if(request('email') && request('email')!==''){
                $query->orWhere('email','=',request('email'));
             }
             if(request('division_id') && request('division_id')!=='all'){
                $query->Where('division_id','=',request('division_id'));
             }
             if(request('district_id') && request('district_id')!=='all'){
                $query->Where('district_id','=',request('district_id'));
             }
             if(request('upazila_id') && request('upazila_id')!=='all'){
                $query->Where('upazila_id','=',request('upazila_id'));
             }
         })
         ->paginate(10);
        $divisions = Division::whereHas('districts.upazilas')->get();
        $districts = District::all();
        $upazilas = Upazila::all();
        return view('users.index', compact('divisions','districts','upazilas','users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $divisions = Division::whereHas('districts.upazilas')->get();
        $exams = Exam::all();
        $universities = University::all();
        $boards = Board::all();
        return view('users.create', compact('divisions','exams','universities','boards'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // return $request->all();
        $validation = $this->validate($request,[
            'name' => 'required',
            'email' => 'required|email',

            'division_id' => 'required',
            'district_id' => 'required',
            'upazila_id' => 'required',
            'address' => 'required',
            'photo' => 'required | mimes:jpeg,jpg,png',
            'cv' => 'required|mimes:doc,pdf,docx',
            'languages' => 'required',
            
            'exam_name' => 'required',
            'university_name' => 'required',
            'board_name' => 'required',
            'exam_result' => 'required',
        ]);
        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->division_id = $request->division_id;
        $user->district_id = $request->district_id;
        $user->upazila_id = $request->upazila_id;
        $user->address = $request->address;
        $user->cv = $this->image_upload($request, 'cv', 'cvs');
        $user->photo = $this->image_upload($request, 'photo', 'photos');
        $user->languages = json_encode($request->languages);
        if($request->have_training == 'have_training') {
            $trainings = [];
            foreach($request->training_names as $k => $v){
                $trainings[$k][] = $request->training_names[$k];
                $trainings[$k][] = $request->training_details[$k];
            }
            $user->trainings = json_encode($trainings);
        }
        $user->save();

        if($user->id){
            foreach($request->exam_name  as $key => $value){
                $education = new UserEducation;
                $education->user_id = $user->id;
                $education->exam_id = $request->exam_name[$key];
                $education->university_id = $request->university_name[$key];
                $education->board_id = $request->board_name[$key];
                $education->exam_result = $request->exam_result[$key];
                $education->save();
            }
        }
        return response()->json([
            'message' => "User Registration successful!"
       ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
            $divisions = Division::with('districts')->get();
            $districts = District::all();
            $upazilas = Upazila::all();
            $exams = Exam::all();
            $universities = University::all();
            $boards = Board::all();
        return view('users.edit' ,compact('user','divisions','exams','universities','boards','districts','upazilas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
           $validation = $this->validate($request,[
            'name' => 'required',
            'email' => 'required|unique:users,email,'.$user->id,

            'division_id' => 'required',
            'district_id' => 'required',
            'upazila_id' => 'required',
            'address' => 'required',
            'photo' => 'mimes:jpeg,jpg,png',
            'cv' => 'mimes:doc,pdf,docx',
            'languages' => 'required',
            
            'exam_name' => 'required',
            'university_name' => 'required',
            'board_name' => 'required',
            'exam_result' => 'required',
        ]);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->division_id = $request->division_id;
        $user->district_id = $request->district_id;
        $user->upazila_id = $request->upazila_id;
        $user->address = $request->address;

        if($request->photo){
            $this->image_delete($user->photo);
            $user->photo = $this->image_upload($request, 'photo', 'photos');
        }
        if($request->cv){
            $this->image_delete($user->cv);
            $user->cv = $this->image_upload($request, 'cv', 'cvs');
        }
      

        $user->languages = json_encode($request->languages);

        $trainings = [];
        if($request->have_training == 'have_training') {
            foreach($request->training_names as $k => $v){
                $trainings[$k][] = $request->training_names[$k];
                $trainings[$k][] = $request->training_details[$k];
            }
            $user->trainings = json_encode($trainings);
        }else{
            $user->trainings = null;
        }
        $user->update();
        if($user->id){
            if($request->exam_name){
                $deleteOld = UserEducation::where('user_id', $user->id)->delete();
                
                    foreach($request->exam_name  as $key => $value){
                        $education = new UserEducation;
                        $education->user_id = $user->id;
                        $education->exam_id = $request->exam_name[$key];
                        $education->university_id = $request->university_name[$key];
                        $education->board_id = $request->board_name[$key];
                        $education->exam_result = $request->exam_result[$key];
                        $education->save();
                    }
                foreach($request->exam_name  as $key => $value){
                //    $education =  UserEducation::where('user_id', $user->id)
                //     ->where('exam_id',$request->exam_name[$key])
                //     ->where('university_id',$request->university_name[$key])
                //     ->where('board_id',$request->board_name[$key])
                //     ->first();
                //     if($education){
                //        return $education->exam_result = $request->exam_name[$key];
                //         $education->save();                        
                //     }else{
                //         $education = new UserEducation;
                //         $education->user_id = $user->id;
                //         $education->exam_id = $request->exam_name[$key];
                //         $education->university_id = $request->university_name[$key];
                //         $education->board_id = $request->board_name[$key];
                //         $education->exam_result = $request->exam_result[$key];
                //         $education->save();
                //     }

                    // return $education;
                    // return $request->exam_result[$key];
                    // UserEducation::updateOrCreate(
                    //     [
                    //         'user_id' => $user->id, 
                    //         'exam_id' =>  $request->exam_name[$key],
                    //         'university_id' => $request->university_name[$key],
                    //         'board_id' => $request->board_name[$key]
                    //     ],
                    //     [ 
                    //         'exam_result' => $request->exam_result[$key]
                    //     ]
                    // );
                }
            }
        }
        return response()->json([
            'message' => "User Update successful!"
       ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }

        /**
     * This is simple custom image upload function
     * @param $request
     * @param $field_name
     * @param $folder
     * @return bool|string
     */
    function image_upload($request, $field_name , $folder, $store_name = '')
    {
        $image = $request->file($field_name);
        $slug = Str::camel($request->name ?? '');
        $location = 'storage/'.$folder;
        if($image !== null){
            $currentDate = Carbon::now()->toDateString();
            if($store_name == ''){
                $image_name = $slug .'_'. $currentDate .'_'. uniqid() .'.'. $image->getClientOriginalExtension();
            }else{
                $image_name = $store_name .'.'.$image->getClientOriginalExtension();
            }
            if(!file_exists($location)){
                mkdir( $location , 0777, true);
            }
            $image->move( $location ,  $image_name);
        }else{
            return null;
        }
        return  $folder.'/'.$image_name;
    }

    /**
     * This is simple custom image delete function
     * @param $file_with_location
     * @return bool
     */
    function image_delete($file_with_location){
        if(file_exists($file_with_location)){
            return unlink($file_with_location);
        }
    }

}
