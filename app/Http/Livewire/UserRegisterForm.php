<?php

namespace App\Http\Livewire;

use Livewire\Component;

class UserRegisterForm extends Component
{

    public $name = "";
    public $email = "";
    protected $rules = [
        'name' => 'required',
        'email' => 'required|email'
    ];
    public function submit(){
        $this->validate();
    }
    public function render()
    {
        return view('livewire.user-register-form');
    }
}
