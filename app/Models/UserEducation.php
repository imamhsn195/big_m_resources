<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserEducation extends Model
{
    protected $fillable = ['user_id','exam_id','university_id','board_id'];

    use HasFactory;
    
    public function exam()
    {
        return $this->belongsTo(Exam::class);
    }
    public function university()
    {
        return $this->belongsTo(University::class);
    }    
    public function board()
    {
        return $this->belongsTo(Board::class);
    }    
}
